package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        boolean toReturn = true;
        int minimalPos = 0;

        /**
         * If lists have not been initializated
         * @throw exception
         */
        if (x == null || y == null)
            throw new IllegalArgumentException();

        /**
         * @if x sequence is bigger than y one
         * @return false
         */
        if (x.size() > y.size()){
            toReturn = false;
            /**
             * @else look through x sequence to find according element in y one.
             * It's important that new suitable element in y sequence has to be placed further than previous one
             */
        } else {
            for(int i = 0; i < x.size(); i++){
                if (y.contains(x.get(i))){
                    if (y.indexOf(x.get(i)) < minimalPos){
                        toReturn = false;
                        break;
                    }
                    minimalPos = y.indexOf(x.get(i));
                } else {
                    toReturn = false;
                    break;
                }
            }
        }
        return toReturn;
    }
}
