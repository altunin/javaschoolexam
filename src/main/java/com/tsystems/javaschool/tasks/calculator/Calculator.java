package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String currentTerm = "";
        List<String> terms = new ArrayList<>();
        List<String> actions = new ArrayList<>();
        String toReturn = "";

        Character currentChar;

        int pointer = 0;
        int indexPointer = 0;
        int code;
        int openBkt = 0;

        /**
         * Check for input mistakes
         */
        if (statement != null && statement.length() > 0) {
            for (int i = 0; i < statement.length(); i++) {

                /**
                 * Check for correct symbols
                 */
                code = Integer.valueOf(statement.charAt(i));
                if (code > 57 || code < 40 || code == 44) {
                    toReturn = null;
                    break;
                }

                /**
                 * Check for correctly pointed brakets
                 */
                if (code == 40)
                    openBkt++;
                if (code == 41)
                    if (openBkt == 0) {
                        toReturn = null;
                        break;
                    } else
                        openBkt--;

                /**
                 * Check for repeating such as .. , *+ , etc.
                 */
                if (code < 48 && (statement.length() - i > 1) && code > 41)
                    if (statement.charAt(i + 1) < 48 && statement.charAt(i + 1) > 41) {
                        toReturn = null;
                        break;
                    }

            }
            if (openBkt != 0)
                toReturn = null;
        } else
            toReturn = null;


        if (toReturn != null){
            /**
             * Filling lists of terms and actions.
             */
            while (pointer < statement.length()) {
                currentChar = statement.charAt(pointer);
                if ((Integer.valueOf(currentChar) >= 48) || (currentChar == '.')) {
                    currentTerm += currentChar;
                } else {
                    if (Integer.valueOf(currentChar) > 41) {
                        if (!currentTerm.equals("")) {
                            terms.add(currentTerm);
                            currentTerm = "";
                        }
                        actions.add(String.valueOf(currentChar));
                    } else {
                        int insidePointer = pointer + 1;
                        String insideTerm = "";
                        while (statement.charAt(insidePointer) != ')'){
                            insideTerm += statement.charAt(insidePointer);
                            insidePointer++;
                        }
//                        insideTerm += statement.charAt(insidePointer);
                        terms.add(evaluate(insideTerm));
                        pointer = insidePointer;
                    }
                }
                pointer++;
            }
            terms.add(currentTerm);

            /**
             * Calculating according to actions taken
             */
            while (!actions.isEmpty()) {
                if (actions.contains("*") || actions.contains("/")) {
                    for (int i = 0; i < actions.size(); i++)
                        if (actions.get(i).equals("*") || actions.get(i).equals("/")) {
                            indexPointer = i;
                            break;
                        }
                    switch (actions.remove(indexPointer)) {
                        case "*":
                            terms.set(indexPointer, String.valueOf(Double.valueOf(terms.get(indexPointer))
                                    * Double.valueOf(terms.remove(indexPointer + 1))));
                            break;
                        case "/":
                            terms.set(indexPointer, String.valueOf(Double.valueOf(terms.get(indexPointer))
                                    / Double.valueOf(terms.remove(indexPointer + 1))));
                            if (terms.get(indexPointer).equals("Infinity"))
                                terms.set(indexPointer, null);
                            break;
                    }
                } else
                    switch (actions.remove(0)) {
                        case "+":
                            terms.set(0, String.valueOf(Double.valueOf(terms.get(0))
                                    + Double.valueOf(terms.remove(1))));
                            break;
                        case "-":
                            terms.set(0, String.valueOf(Double.valueOf(terms.get(0))
                                    - Double.valueOf(terms.remove(1))));
                            break;
                    }


            }

            toReturn = terms.get(0);
        }
        /**
         * Formatting of output
         */
        if (toReturn != null)
            if (Double.valueOf(toReturn) % 1 == 0) {
                double doubleToReturn = Double.valueOf(toReturn);
                toReturn = String.valueOf((int) doubleToReturn);
            } else {
                if (toReturn.length() - toReturn.indexOf('.') > 5){
                    toReturn = toReturn.substring(0, toReturn.indexOf('.') + 4);
                }

            }


        return toReturn;
    }

}
