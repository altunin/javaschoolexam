package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        /**
         * Limits amounts of pyramid rows to 500
         */
        if (inputNumbers.size() > 125250)
            throw new CannotBuildPyramidException();

        /**
         * If null values exist, exception will be thrown
         */
        for(Integer i : inputNumbers)
            if (i == null)
                throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);
        Collections.reverse(inputNumbers);

        int iterator = 0;
        int amount = inputNumbers.size();
        int diff = 1;
        int rows;
        int columns;

        /**
         * Checks if pyramid can be built by given params
         */
        while (amount > 0){
            amount -= diff;
            diff++;
        }
        if (amount < 0)
            throw new CannotBuildPyramidException();


        /**
         * Calculating rows and columns number
         */
        rows = (int) (Math.sqrt(8 * inputNumbers.size() + 1) - 1)/2;
        columns = 2*rows - 1;

        /**
         * Massive initialization
         */
        int[][] pyramid = new int[rows][columns];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                pyramid[i][j] = 0;

        /**
         * Arranging numbers according their positions
         */
        for (int i = rows - 1; i > -1; i--)
            for (int j = columns - rows + i; j >= rows - 1 - i; j -= 2) {
                pyramid[i][j] = inputNumbers.get(iterator);
                iterator++;
            }

        return pyramid;
    }


}
